<?php namespace Brandmovers\Twitter;

class TwitterSearchClient
{
    protected $endPoint = 'https://api.twitter.com/1.1/search/tweets.json';
    protected $errorCode;
    protected $errorMessage;
    protected $token;
    protected $type = 'recent';
    protected $count = 100;
    protected $query;
    protected $numTweetsLastRequest = 0;
    protected $nextRequest;
    protected $lastRequest;
    protected $totalTweets = 0;
    protected $minTweetId = 0;

    public function __construct($config = array()) {
        if (isset($config['token'])) $this->token = $config['token'];
        if (isset($config['type'])) $this->type = $config['type'];
        if (isset($config['count'])) $this->count = $config['count'];
        if (isset($config['query'])) $this->query = $config['query'];
    }

    public function setToken($token) {
        $this->token = $token;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setCount($count) {
        $this->count = $count;
    }

    public function numTweetsLastRequest() {
        return $this->numTweetsLastRequest;
    }

    public function lastRequest() {
        return $this->lastRequest;
    }

    public function nextRequest() {
        return $this->nextRequest;
    }

    public function totalTweets() {
        return $this->totalTweets;
    }

    public function errorCode() {
        return $this->errorCode;
    }

    public function errorMessage() {
        return $this->errorMessage;
    }

    // Return true if the authentication token has expired.
    public function isAuthTokenExpired() {
        return ($this->errorCode == 89);
    }

    // Query to search, should not be URL encoded.
    public function setQuery($query) {
        $this->query = $query;
    }

    // Performs a search. If called iteratively it will perform a progessive search.
    public function search() {
        // Reset error buffers.
        $this->errorCode = NULL;
        $this->errorMessage = NULL;

        // Assemble the URL.
        if ($this->nextRequest) {
            $this->lastRequest = $this->nextRequest;
            $url = $this->endPoint.$this->nextRequest;
        } elseif ($this->lastRequest == NULL) {
            $query = array(
                'count' => $this->count,
                'result_type' => $this->type,
                'q' => $this->query,
            );
            $this->lastRequest = "?".http_build_query($query, '', '&');
            $url = $this->endPoint.$this->lastRequest;
        } else {
            $this->numTweetsLastRequest = 0;
            return array();
        }

        // Construct request.
        $curl = curl_init();

        // Build the request headers.
        $headers = array(
            'Authorization: Bearer '.$this->token->token
        );

        // Build the cURL options.
        // Note, CURLOPT_POST must be set before other headers or it will
        // break any headers set before it.
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($curl, $options);

        print "REQUEST: ".$url."\n";
        $response = curl_exec($curl);
        curl_close($curl);

        // Evaluate the response.
        $response = @json_decode($response);

        // Check for and deal with any errors.
        if (isset($response->errors)) {
            $this->errorCode = $response->errors[0]->code;
            $this->errorMessage = $response->errors[0]->message;
            throw new \Exception($this->errorMessage);
        }

        // Store the number tweets returned.
        $this->numTweetsLastRequest = count($response->statuses);
        $this->totalTweets += $this->numTweetsLastRequest;

        // Determine the next request
        if ($this->numTweetsLastRequest > 0) {
            foreach ($response->statuses as $tweet) {
                if ($this->minTweetId == 0 || $this->minTweetId > $tweet->id) {
                    $this->minTweetId = $tweet->id;
                }
            }
            $query = array(
                'count' => $this->count,
                'result_type' => $this->type,
                'q' => $this->query,
                'max_id' => ($this->minTweetId - 1)
            );
            $this->nextRequest = "?".http_build_query($query, '', '&');
        } else {
            $this->nextRequest = NULL;
        }

        // Return the tweets.
        return $response->statuses;
    }


}