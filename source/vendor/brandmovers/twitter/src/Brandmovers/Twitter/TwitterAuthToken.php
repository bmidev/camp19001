<?php namespace Brandmovers\Twitter;

use Illuminate\Database\Eloquent\Model;

class TwitterAuthToken extends Model {

	// #########################################################################
	// PROPERTIES
	// #########################################################################

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'twitter_auth_tokens';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that can be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = ['token'];

	// #########################################################################
	// ACCESSORS AND MUTATORS
	// #########################################################################

}
