<?php namespace Brandmovers\Twitter;

use Illuminate\Database\Eloquent\Model;

class TwitterRequestLog extends Model {

	// #########################################################################
	// PROPERTIES
	// #########################################################################

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'twitter_request_logs';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that can be mass assigned.
	 *
	 * @var array
	 */
	protected $fillable = ['status', 'message'];

	// #########################################################################
	// ACCESSORS AND MUTATORS
	// #########################################################################

}
