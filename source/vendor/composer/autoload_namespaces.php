<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PhpSpec' => array($vendorDir . '/phpspec/phpspec/src'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Mcamara\\LaravelLocalization' => array($vendorDir . '/mcamara/laravel-localization/src'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Instagram' => array($vendorDir . '/php-instagram-api/php-instagram-api'),
    'Dotenv' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Brandmovers\\Twitter' => array($vendorDir . '/brandmovers/twitter/src'),
    'Brandmovers\\Contests' => array($vendorDir . '/brandmovers/contests/src'),
);
