<?php namespace App\Console\Commands;

use Brandmovers\Social\TintUp\TintUpImporter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class TintUpAgentImport extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tintup-agent:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query TintUp\'s API and store the posts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set("max_execution_time", 6000);
        $importer = new \Brandmovers\Social\TintUp\TintUpImporter();
        $importer->import();
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            // ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['full', null, InputOption::VALUE_NONE, 'Retrieve all tweets.', null],
        ];
    }

}
