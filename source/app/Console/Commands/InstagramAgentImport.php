<?php namespace App\Console\Commands;

use Brandmovers\Social\Instagram;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class InstagramAgentImport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'instagram-agent:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Search Instagram\'s API and store the posts.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$grammy = new \Brandmovers\Social\Instagram\InstagramImporter();
		$grammy->import();
		return;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		//['full', null, InputOption::VALUE_NONE, 'Retrieve all tweets.', null],
		];
	}

}
