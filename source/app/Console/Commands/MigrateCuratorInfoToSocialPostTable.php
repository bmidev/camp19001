<?php
namespace App\Console\Commands;

use App\Posts as CuratorPost;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Brandmovers\Social\Post as SocialPost;
use Illuminate\Support\Collection;

class MigrateCuratorInfoToSocialPostTable extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api-curator:migrate-to-post-table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate to post table from curator temp table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        // 2018-05-15 07:10:02

        CuratorPost::query()->where('imported', '!=', 1)->chunk(300, function(Collection $posts){
            $this->migrateCuratorPost($posts);
        });
    }

    /**
     * Migrate Curator Post
     * [Import Chunk]
     *
     * @param Collection $posts
     */
    protected function migrateCuratorPost(Collection $posts){
        $posts->map(function(CuratorPost $post){
            $attributes = collect([
                'url'                   => $post->url,
                'media_type'            => !empty($post->video) ? 'video' : 'image',
                'image'                 => $post->image,
                'image_thumb'           => $post->image_thumbnail ?: $post->image,
                'video'                 => $post->video,
                'comments'              => $post->text,
                'author_name'           => $post->user_name,
                'author_image'          => $post->user_image,
                'author_username'       => $post->user_screen_name,
                'author_profile'        => $post->user_url,
                'network'               => $post->network_name,
                'network_id'            => $post->source_identifier,
                'post_date'             => $post->source_created_at,
                'show_gallery'          => 0,
                'valid_entry'           => 1,
                'pepperidge_inspired'   => (strtolower(trim($post->network_name)) == 'pepperidge') || (strtolower(trim($post->user_screen_name)) == 'pepperidgefarm') || (strtolower(trim($post->user_name)) == 'pepperidgefarm')
            ]);

            if($post = SocialPost::query()->where('network_id', $attributes->get('network_id'))->first()){
                return $post->update($attributes->except(['network_id'])->toArray());
            }

            SocialPost::query()->firstOrCreate($attributes->toArray());
        });

        CuratorPost::query()->whereIn('source_identifier', $posts->pluck('source_identifier')->toArray())->update([
            'imported' => 1
        ]);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            // ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
            //['full', null, InputOption::VALUE_NONE, 'Retrieve all tweets.', null],
        ];
    }
}

