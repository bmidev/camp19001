<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class APICuratorImport extends Command {

    protected $signature = 'api-curator:import';
    protected $description = 'Instagram and Twitter Hashtag Pull';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        ini_set("max_execution_time", 6000);
        $apiCurator = new \Brandmovers\Social\APICurator\Importer();
        $apiCurator->import();
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
            // ['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
            //['full', null, InputOption::VALUE_NONE, 'Retrieve all tweets.', null],
        ];
    }
}
