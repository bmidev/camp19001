<?php namespace App\Console\Commands;

//use Brandmovers\Contests\PostValidator;
use Brandmovers\Twitter\TwitterAppAuthenticator;
use Brandmovers\Twitter\TwitterPost;
use Brandmovers\Twitter\TwitterRequestLog;
use Brandmovers\Twitter\TwitterSearchClient;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Brandmovers\Social\Post;

class TwitterAgentImport extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'twitter-agent:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Search Twitter\'s API and store the tweets.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        // This command should be run every 5 minutes and Twitter only allows
        // 450 requests every 15 minutes. Our max requests per run is
        // 450 / 3 = 150 requests.
        $maxRequests = 150;

        // This is the number of requests in each processing batch.
        $requestsPerBatch = 10;

        // Get the search query, start and end date.
        $query = Config::get('twitter.search_query');
        $startDate = strtotime(Config::get('twitter.start_date'));
        $endDate = strtotime(Config::get('twitter.end_date'));

        // Create post validator and load up contest rules.
        //$validator = new PostValidator();
        //$validator->addRules(Config::get('contest.rules'));

        // Validate that we are inside the promotional window.
        //if (time() < $startDate || time() >= ($endDate + 3600)) return;

        // This script runs in 2 modes, full history and diff. In full mode
        // the script will attempt to get all tweets in the current promotional
        // time window or until it hits the API limit. In diff mode, it will
        // attempt to only the most recent tweets since the last time it ran.
        //$mode = ($this->option('full')) ? 'full' : 'diff';
		$mode = 'diff';
        
        // Create an authenticator.
        $authenticator = new TwitterAppAuthenticator(Config::get('twitter'));

        // Counters
        $tweetsSaved = 0;

        // Get an authentication token.
        $authToken = $authenticator->authToken();

        // Get the last request.
        $lastRequest = TwitterRequestLog::orderBy('created_at', 'desc')->first();

/*
        // Don't run if the last request status was RUNNING.
        if ($lastRequest != NULL && $lastRequest->status == 'RUNNING') {
            // If it's been more than 30 minutes since it's been updated
            // then we can safely assume there was an error, otherwise
            // we will halt.
            if ((time() - strtotime($lastRequest->updated_at)) <= (30*60)) {
                return;
            } else {
                $mode = 'full';
                $body = "ERROR twitter-agent:import \n\n";
                $body .= "It's been 30 minutes and the last request is still marked at RUNNING. \n";
                Mail::send('notifications.email-error', array('content' => $body), function($message) {
                    $message->from('alert@brandmovers.com');
                    $message->to(Config::get('notifications.developer_email'))->subject('ERROR twitter-agent:import');
                });
            }
        }
*/

        // Create a new request.
        $currentRequest = new TwitterRequestLog(array(
            'status' => 'RUNNING', 
            'message' => 'Initializing'
        ));
        $currentRequest->save();

        // If last request failed, we have to pull all tweets.
        if ($lastRequest == NULL) {
            $mode = 'full';
        } elseif ($lastRequest->status == 'FAILED') {
            $mode = 'full';
        }

        // Create and configure a Twitter search client.
        $client = new TwitterSearchClient(array(
            'token' => $authToken,
            'query' => $query,
        ));

        // Search and looping variables.
        $continue = TRUE;
        $savesPerBatch = 0;
        $outsideWindowPerBatch = 0;
        $postsPerBatch = 0;
        $iteration = 0;
		

        // Search till we hit a criteria to stop.
        while ($continue) {
            $iteration++;

            // Hit the Twitter API using our Twitter client.
            try {
                $response = $client->search();
            } catch (Exception $e) {
                // If something failed, log and email it.
                $currentRequest->status = 'FAILED';
                $currentRequest->message = 'ERROR: '.$client->errorMessage();
                $currentRequest->save();
                if ($client->isAuthTokenExpired()) {
                    // Remove the expired token.
                    $authenticator->invalidateAuthToken($authToken);
                }

                // Send email on error.
                $body = "ERROR twitter-agent:import \n\n";
                $body .= $client->errorMessage()." \n";
                Mail::send('notifications.email-error', array('content' => $body), function($message) {
                    $message->from('alert@brandmovers.com');
                    $message->to(Config::get('notifications.developer_email'))->subject('ERROR twitter-agent:import');
                });
                return;
            }

            // Stop if we didn't get any tweets.
            if (sizeof($response) == 0) {
                $continue = FALSE;
                continue;
            }

            // Process the tweets.
            foreach ($response as $tweet) {
                // Count all posts in this batch.
                $postsPerBatch++;

                // Check to make sure the tweet isn't outside the valid promotional window.
                //if (strtotime($tweet->created_at) < $startDate || strtotime($tweet->created_at) > $endDate) {
                //    $outsideWindowPerBatch++;
                //    continue;
                //}

                // Attempt to find this tweet, if we find one then skip it.
                $pastTweet = Post::where('network_id', '=', $tweet->id_str)->where('network', Post::NETWORK_TWITTER)->first();
                if ($pastTweet != NULL) continue;

                // Create and store this new tweet.
                

				// Extract the first specified media from tweet
				$mediaUrl = null;
				$mediaThumbUrl = null;

				if( isset($tweet->entities->media) && is_array($tweet->entities->media) ) {
					$media = $tweet->entities->media[0];

                    if( $media->type == "photo" ) {
                        $mediaUrl = $media->media_url . ":large";
                        $mediaThumbUrl = $media->media_url . ":thumb";
                    }
                    else {
                        dd($tweet);

                    }
				}

	            $post = new Post([
					'network'			=> Post::NETWORK_TWITTER,
					'network_id'		=> $tweet->id_str,
					'post_date' 		=> date('Y-m-d H:i:s', strtotime($tweet->created_at)),
					'url'				=> "http://twitter.com/" . urlencode($tweet->user->screen_name) . "/status/" . urlencode($tweet->id),
					'image'				=> $mediaUrl,
					'image_thumb'		=> $mediaThumbUrl,
					'comments'			=> $tweet->text,
					'author_name'		=> $tweet->user->name,
					'author_image'		=> $tweet->user->profile_image_url,
					'author_username'	=> $tweet->user->screen_name,
					'author_profile'	=> "http://twitter.com/" . urlencode($tweet->user->screen_name)
				]);

				// Validate this post
				$post->validate();
				
                // Save the tweet.
                $post->save();
                $tweetsSaved++;
                $savesPerBatch++;
            } // end foreach


            // Determine if we've hit the end of a batch.
            if ($iteration % $requestsPerBatch == 0) {
                // If we haven't gotten any new tweets in the last batch and we are in diff
                // mode then we stop.
                if ($savesPerBatch == 0 && $mode == 'diff') $continue = false;

                // If all posts are outside the valid window we stop.
                if ($outsideWindowPerBatch > 0 && $outsideWindowPerBatch == $postsPerBatch) $continue = false;

                // If we hit the API limit then we stop.
                if ($iteration >= $maxRequests) $continue = false;

                // Reset batch counters.
                $savesPerBatch = 0;
                $postsPerBatch = 0;
                $outsideWindowPerBatch = 0;

                // Update the twitter log.
                $currentRequest->message = 'Tweets Processed: '.number_format($client->totalTweets()).
                    '. Tweets Saved: '.number_format($tweetsSaved);
                $currentRequest->save();
            } 
        } // end while.

        // Complete the request.
        $currentRequest->status = 'COMPLETED';
        $currentRequest->message = 'Tweets Processed: '.number_format($client->totalTweets()).
            '. Tweets Saved: '.number_format($tweetsSaved);
        $currentRequest->save();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
		['full', null, InputOption::VALUE_NONE, 'Retrieve all tweets.', null],
		];
	}

}
