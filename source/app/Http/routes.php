<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale()], function() {

    Route::get('/', 'IndexController@index');
    //function () {
    //    return view('index');
    //});


    Route::get('/official-rules', [
        'uses' => 'IndexController@rules',
        'as' => 'rules'
    ]);
    //[ 'as' => 'rules', function () {
    //    return view('rules');
    //}]);

});


Route::get('gallery', 'GalleryController@index');


// Admin Panel
Route::group(['namespace' => 'Admin', 'middleware' => 'goldfish.admin'], function() {

    Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {

        Route::get('dashboard', [
            'as'			=> 'admin.dashboard',
            'uses' 			=> 'DashboardController@index'
        ]);

        // Manually trigger Instagram import
        Route::get('import-instagram', [
            'as'			=> 'admin.instagram',
            'uses' 			=> 'DashboardController@importInstagram'
        ]);

        // Manually trigger Twitter import
        Route::get('import-twitter', [
            'as'			=> 'admin.twitter',
            'uses' 			=> 'DashboardController@importTwitter'
        ]);

        Route::get('posts/moderate', [
            'as'            => 'admin.posts.moderation',
            'uses'          => 'PostsController@moderation'
        ]);
        Route::post('posts/{id}/moderate', 'PostsController@moderate');
        Route::post('posts/{id}/winners', 'PostsController@winners');
        Route::resource('posts', 'PostsController', ['only' => ['index']]);
        Route::resource('inspired_content', 'InspiredContentController');
    });

    // Admin Login/Logout (This needs to come after the rules above, and out of the middleware)
    Route::controller('admin', 'AuthController');

});

