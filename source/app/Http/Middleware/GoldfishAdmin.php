<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\App;

class GoldfishAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( App::environment('production') ) {

            // These rules only apply on production
            // Redirect if on GoldfishSmiles.com domain
            if( strpos($request->root(), config('app.domain')) ) {
                return Redirect::to('https://camp17001.brandmovers.co/admin');
            }

            // Force SSL
            if( !$request->secure() ) {
                return Redirect::secure($request->path());
            }

        }


        return $next($request);
    }
}
