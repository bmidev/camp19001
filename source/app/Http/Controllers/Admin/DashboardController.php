<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brandmovers\Social\Post;
use Illuminate\Support\Facades\Config;
use App\Console\Commands\TwitterAgentImport;
use LaravelAnalytics;
use Carbon\Carbon;


class DashboardController extends Controller {

	/*
	|---------------------------------------------------------------------------
	| Admin Dashboard Controller
	|---------------------------------------------------------------------------
	|
	| Primary admin controller displaying dashboard stats
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Admin dashboard
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		if( $request->has('start_date') ) {
			$startDate = Carbon::createFromFormat('m/d/Y', $request->input('start_date'));
		}
		else{
			$startDate = Carbon::today()->subDays(30);
		}

		if( $request->has('end_date') ) {
			$endDate = Carbon::createFromFormat('m/d/Y', $request->input('end_date'));
		}
		else {
			$endDate = Carbon::today();
		}

		$totalEntries = Post::where('valid_entry', true)
			->where('pepperidge_inspired', false)
			->whereBetween('post_date', [$startDate, $endDate])
			->count();

		$networkSummary = Post::networkSummary($startDate, $endDate);
		$mediaTypeSummary = Post::mediaTypeSummary($startDate, $endDate);

		$analytics = [];
		$analytics['pageViews'] = LaravelAnalytics::getVisitorsAndPageViewsForPeriod($startDate, $endDate);

		$chartColors = [
			'color' 	=> ['#E67F22', '#2A80B9', '#27AE61'],
			'highlight'	=> ['#F1C50E', '#2C97DD', '#51D88B']
		];

		return view('admin.dashboard.index', [
			'totalEntries'		=> $totalEntries,
			'networkSummary'	=> $networkSummary,
			'mediaTypeSummary'	=> $mediaTypeSummary,
			'analytics'			=> $analytics,
			'chartColors'		=> $chartColors,
			'filters'			=> [
				'startDate'	=> $startDate,
				'endDate'	=> $endDate
			]
		]);
	}



	
	public function importInstagram() {
		echo "<pre>";
		$grammy = new \Brandmovers\Social\Instagram\InstagramImporter();
		$grammy->import();
		return;

	}
	
	
	public function importTwitter() {
		echo "<pre>";
		$tw = new TwitterAgentImport();
		$tw->fire();
		return;

	}

}
