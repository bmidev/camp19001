<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Brandmovers\Social\Post;
use Illuminate\Support\Facades\Config;


class PostsController extends Controller
{

    /**
     * Admin Posts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Post::query();
        $query = $query->where('pepperidge_inspired', false); // Always exclude inspired content

        // Period
        $searchPeriod = $request->input('period', false);
        if( $searchPeriod !== false && isset($periods[$searchPeriod]) ) {
            $query = $query->whereBetween('post_date', [$periods[$searchPeriod]['start'], $periods[$searchPeriod]['end']]);
        }

        // Moderation Check
        if( $request->has('approved') ) {
            if( $request->input('approved') == 'approved' ) {
                $query = $query->where('approved', true);
            }
            elseif( $request->input('approved') == 'rejected' ) {
                $query = $query->where('approved', false)->where('reviewed', true);
            }
            elseif( $request->input('approved') == 'pending' ) {
                $query = $query->where('reviewed', false);
            }
        }


        // Valid Entry Checks
        if( $request->has('valid') ) {
            if( $request->input('valid') !== 'all' ) {
                $query = $query->where('valid_entry', $request->input('valid'));
            }
        }
        else {
            $query = $query->where('valid_entry', true);  // Default to valid entries only
        }

        // Contest Eligible
        if( $request->has('contest_eligible') && $request->input('contest_eligible') != 'all' ) {
            $query = $query->where('contest_eligible', $request->input('contest_eligible'));
        }

        // Media Type
        if( $request->has('media_type') && $request->input('media_type') != 'all' ) {
            $query = $query->where('media_type', $request->input('media_type'));
        }


        // Search/Export/Winner Options
        $submitAction = $request->input('submit');
        if( $submitAction == "winner" ) {
            $query = $query->where('valid_entry', 1)->orderByRaw("RAND()")->paginate(1);
        }
        elseif( $submitAction == "export" ) {
            return $this->export($query->get()->toArray());
        }
        else {
            $query = $query->orderBy('post_date', 'DESC')->paginate(20);
        }


        return view('admin.posts.index', [
            'posts' 		=> $query,
            'filters'       => [
                'approved'          => $request->input('approved'),
                'valid'             => $request->input('valid', 1),
                'contest_eligible'  => $request->input('contest_eligible'),
                'media_type'        => $request->input('media_type')
            ],
            'submitAction'	=> $submitAction,
        ]);

    }


    /**
     * Moderation view for moderating new social posts
     *
     * @param Request $request
     */
    public function moderation(Request $request) {

        $posts = Post::where('reviewed', false)->where('valid_entry', true)->orderBy('post_date', 'asc')->get();

        return view('admin.posts.moderation', compact('posts'));
    }


    /**
     * Moderate a social post
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function moderate(Request $request, $id) {

        $post = Post::findOrFail($id);

        $post->approved = $request->input('approved', false);
        $post->contest_eligible = $request->input('contest_eligible', false);
        $post->show_gallery = $request->input('show_gallery', false);
        $post->reviewed = true;

        $post->save();

        return response()->json($post);
    }


    /**
     * Mark as contest or sweepstakes winner
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function winners(Request $request, $id) {

        $post = Post::findOrFail($id);

        $post->sweepstakes_winner = (bool)$request->input('sweepstakes_winner', false);
        $post->contest_winner = (bool)$request->input('contest_winner', false);
        $post->contest_eligible = $request->input('contest_eligible', false);
        $post->show_gallery = $request->input('show_gallery', false);

        $post->save();
        return response()->json($post);
    }


    /**
     * Export posts using the same search filtering from the index action
     */
    private function export($posts) {
        $headers = [
            'Content-type'        => 'application/csv',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=Pepperidge_Farm_Export_' . date('Y-m-d_H-i') . '.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        # add headers for each column in the CSV download
        array_unshift($posts, array_keys($posts[0]));

        $callback = function() use ($posts) {
            $FH = fopen('php://output', 'w');
            foreach ($posts as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return \Response::stream($callback, 200, $headers);
    }


}