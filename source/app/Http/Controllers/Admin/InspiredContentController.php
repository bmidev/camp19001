<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Storage;

use Brandmovers\Social\Post;


class InspiredContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::where('pepperidge_inspired',1)->orderBy('id', 'desc')->get();

        return view('admin.inspired_content.index', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.inspired_content.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'comments'             => 'required',
        );

        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('admin.inspired_content.create')
                ->withErrors($validator)
                ->withInput();
        } else {

            $image_url = $this->uploadFile($request, 'photo');
            $video_url = $this->uploadFile($request, 'video');

            $media_type = $video_url ? "video" : "image";

            $post_values = [
                'pepperidge_inspired' => 1,
                'author_name' => 'Respect the bun',
                'valid_entry' => 1,
                'approved' => 1,
                'media_type' => $media_type,
                'image' => $image_url,
                'image_thumb' => $image_url,
                'video' => $video_url,
                'network' => 'Pepperidge',
                'network_id' => time(),
                'post_date' => date('Y-m-d H:i:s')
            ];

            $post = Post::create(array_merge($request->all(),$post_values));

            $post->show_gallery = true;
            $post->approved = true;
            $post->reviewed = true;
            $post->save();


            // redirect
            \Session::flash('message', 'Successfully added post');
            return redirect()->route('admin.inspired_content.index');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return redirect()->route('admin.inspired_content.index');
        /*$post = Post::find($id);

        return view('admin.inspired_content.show', [
            'post' => $post
        ]);*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('admin.inspired_content.edit', [
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        $rules = array(
            'comments'             => 'required',
        );
        $validator = \Validator::make(\Input::all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('admin.inspired_content.show', [
                'post' => $post
            ])
                ->withErrors($validator)
                ->withInput();
        }

        $post_values = $request->all();

        if ($request->hasFile('file'))
        {
            $post_values['image'] = $this->uploadImage($request);
            $post_values['image_thumb'] = $post_values['image'];
        }

        $post->update($post_values);

        // redirect
        \Session::flash('message', 'Successfully updated post');
        return redirect()->route('admin.inspired_content.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        \Session::flash('message', 'Inspiration post deleted');
        return redirect()->route('admin.inspired_content.index');
    }


    /**
     * Upload an image, save it to S3, and return the URL
     *
     * @param Request $request
     * @return string
     */
    public function uploadFile(Request $request, $name)
    {
        if( !$request->hasFile($name) ) {
            return null;
        }

        $image = $request->file($name);
        $fileHash = sha1_file($image->getRealPath());
        $fileKey = strtolower(config('app.project_code'). '/' .\App::environment().'/inspired-content/'.date('YmdHis').'-'.$fileHash.'.'.$image->getClientOriginalExtension());

        $s3 = Storage::disk('s3');
        $s3->put($fileKey, file_get_contents($image->getRealPath()));

        return "https://bm-projects-public.s3.amazonaws.com/" . $fileKey;
    }

}
