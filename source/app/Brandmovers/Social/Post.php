<?php namespace Brandmovers\Social;

use Illuminate\Database\Eloquent\Model;
use Brandmovers\Promotion\Period;
use Carbon\Carbon;


class Post extends Model {

	protected $table = "social_posts";
	
	const NETWORK_TWITTER = "Twitter";
	const NETWORK_INSTAGRAM = "Instagram";

	const MEDIA_TYPE_IMAGE = "image";
	const MEDIA_TYPE_VIDEO = "video";
	
	/**
	 * Mass assignment white-list
	 * 
	 * @var array
	 */
	protected $fillable = [
		'url', 'media_type', 'image', 'image_thumb', 'video', 'title', 'comments', 'author_name', 'author_image', 'author_username', 'author_profile',
		'network', 'network_id', 'post_date', 'contest_eligible', 'contest_winner', 'sweepstakes_winner', 'pepperidge_inspired', 'valid_entry', 'show_gallery'
	];


	/**
	 * Save a post, ignoring if it has a duplicate entry constraint violation.  
	 * 
	 * @return OpenTable\Social\Post
	 */
	public function saveIgnoreDuplicates() {

		try {
			return $this->save();
		}
		catch(\Exception $e) {
			// Catch duplicate entry constraint and ignore
			if( $e->errorInfo[0] == "23000" ) {
				// Continue
				return false;
			}
			else {
				throw $e;
			}
		}
		
	}
	
	
	/**
	 * Scope: Unprocessed Entries
	 */
	public function scopeUnprocessed($query) {
		return $query->where('processed', '=', 0);
	}
	
	
	public function scopeWithinDates($query, $startDate, $endDate) {
		return $query->whereBetween('post_date', [$startDate, $endDate]);
	}


	/**
	 * Get posts for display in public gallery
	 *
	 * @param $query
	 * @return mixed
	 */
	public function scopeGallery($query) {
		return $query->orderBy('post_date', 'desc')->where('show_gallery', true)->where('valid_entry', true);
	}
	
	
	/**
	 * Process posts into entries by the specified batch size per run
	 * 
	 * @param int $valid - Reference parameter will update with number of valid entries
	 * @param int $invalid = Reference parameter will update with number of invalid entries
	 * @return int
	 */
	public static function processEntries($batchSize = 1000, &$valid = NULL, &$invalid = NULL) {
		
		$batch = self::where('processed', '=', 0)->take($batchSize)->get();
		$valid = 0;
		$invalid = 0;

		foreach($batch as $p) {
			
			// Manually set network since the feed does not for some reason
			$p->network = $p->getNetwork();
			
			if( $p->validate() ) {
				$p->valid_entry = true;
				$valid++;
			}
			else {
				$p->valid_entry = false;
				$invalid++;
			}
			
			$p->processed = true;
			$p->save();
		}
		
		return count($batch);

	}
	
	
	/**
	 * Process the social post and validate them against sweepstakes rules.
	 * Check if the entry was valid, if not copy any entry validation errors
	 * to the post for later reference (if needed)
	 * 
	 * @return bool
	 */
	public function validate() {
		$this->valid_entry = 1;

		// Make sure an image is specified 
		if( $this->image == "" ) {
			$this->errors = "No image specified.";
			$this->valid_entry = 0;
			return false;
		}
		
		// Make sure the date is within a given promotion period range
		if( !Period::isWithinPeriod($this->post_date) ) {
			$this->errors = "Not within a promotion period.";
			$this->valid_entry = 0;
			return false;
		}
		
		// Validate one entry per method/day/user
		if( $this->validateOnePerDay() ) {
			$this->errors = "One entry per day";
			$this->valid_entry = 0;
			return false;	
		}
		
		return true;
		
	}
	
	
	/**
	 * Validate one entry per method/day/user
	 * 
	 * @return bool
	 */
	public function validateOnePerDay() {
		$date = Carbon::createFromFormat('Y-m-d H:i:s', $this->post_date);
		
		return self::where('post_date', '>', $date->toDateString())
			->where('post_date', '<', $date->addDay()->toDateString())
			->where('network', $this->network)
			->where('author_username', $this->author_username)
			->exists();
	}
	
	
	
	/**
	 * Validate one entry per period
	 * 
	 * @return bool
	 */
	public function validateOnePerPeriod() {
		$period = Period::getPeriodContained($this->post_date);

		return self::where('post_date', '>=', $period['start'])
			->where('post_date', '<=', $period['end'])
			->where('network', $this->network)
			->where('author_username', $this->author_username)
			->exists();
	}
	
	
	
	/**
	 * Search the content of a post and extract all #hashtags and @mentions
	 * 
	 * @param array $validTags
	 * @return string
	 */
	public function extractTagsAndMentions() {
		$matches = [];
		preg_match_all("/[#|@]+([a-zA-Z0-9_]+)/", $this->comments, $matches);
		return $matches[0];
	}
	
	
	/**
	 * Get the network the post was provided from
	 * 
	 * CSV data from TintUp does not include this for some reason, the
	 * network field is always blank.  Best solution is to figure 
	 * it out from the author's profile URL.
	 * 
	 * @return string
	 */
	public function getNetwork() {

		if( strpos($this->author_profile, 'twitter.com') !== FALSE ) {
			return self::NETWORK_TWITTER;
		}
		elseif( strpos($this->author_profile, 'instagram.com') !== FALSE ) {
			return self::NETWORK_INSTAGRAM;
		}
		
		return null;

	}
	
	
	/**
	 * Network Summary
	 * Get breakdown of posts by network
	 * 
	 * Example Return Value:
	 * [
	 * 	 [
	 *     'method'  => 'Twitter'
	 *     'total'   => 89,458
	 *   ]
	 *   ...
	 * ]
	 * 
	 * @return array
	 */
	public static function networkSummary($startDate = null, $endDate = null) {
		$query = self::selectRaw('network, COUNT(id) AS total')
			->where('valid_entry', 1)
			->where('pepperidge_inspired', false)
			->groupBy('network');

		if( $startDate ) {
			$query = $query->where('post_date', '>=', $startDate);
		}
		if( $endDate ) {
			$query = $query->where('post_date', '<=', $endDate);
		}

		return $query->get()->toArray();
	}


	/**
	 * Media Type Summary
	 * Get breakdown of posts by media type
	 *
	 * Example Return Value:
	 * [
	 * 	 [
	 *     'media_type'  => 'video'
	 *     'total'   => 89,458
	 *   ]
	 *   ...
	 * ]
	 *
	 * @return array
	 */
	public static function mediaTypeSummary($startDate = null, $endDate = null) {
		$query = self::selectRaw('media_type, COUNT(id) AS total')
			->where('valid_entry', 1)
			->where('pepperidge_inspired', false)
			->groupBy('media_type');

		if( $startDate ) {
			$query = $query->where('post_date', '>=', $startDate);
		}
		if( $endDate ) {
			$query = $query->where('post_date', '<=', $endDate);
		}

		return $query->get()->toArray();
	}


	/**
	 * Array for JSON to be used within a publicly visible service (contains only public social post data)
	 *
	 * @param int $options
	 * @return array
	 */
	public function toPublicJson($options = 0) {
		return array(
			'network' 		=> $this->network,
			'network_id'	=> $this->network_id,
			'url'			=> $this->url,
			'media_type'	=> $this->media_type,
			'image'			=> $this->image,
			'thumbnail'		=> $this->image_thumb,
			'video'			=> $this->video,
			'comments'		=> $this->comments,
			'author'		=> array(
				'name'			=> $this->author_name,
				'avatar'		=> $this->author_image,
				'username'		=> $this->author_username,
				'profile_url'	=> $this->author_profile
			),
			'banner'		=> array(
				'contest_winner'		=> $this->contest_winner,
				'sweepstakes_winner'	=> $this->sweepstakes_winner,
				'pepperidge_inspired'		=> $this->pepperidge_inspired
			),
			'date'			=> $this->post_date
		);
	}


	/**
	 * Determine if post was an image post
	 *
	 * @return bool
	 */
	public function isImage() {
		return ($this->media_type == self::MEDIA_TYPE_IMAGE && $this->image);
	}

	/**
	 * Determine if post was a video post
	 *
	 * @return bool
	 */
	public function isVideo() {
		return ($this->media_type == self::MEDIA_TYPE_VIDEO && $this->video);
	}

}
