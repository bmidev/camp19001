<?php namespace Brandmovers\Social\TintUp;

use Illuminate\Database\Eloquent\Model;


class TintUpRequestLog extends Model
{
    const STATUS_RUNNING = "RUNNING";
    const STATUS_COMPLETE = "COMPLETED";
    const STATUS_FAILED = "FAILED";

    protected $table = "tintup_request_logs";



    /**
     * Get the most recent request made
     *
     * @return InstagramRequestLog
     */
    public static function getLastRequest() {
        return self::orderBy('created_at', 'desc')->where('status', self::STATUS_COMPLETE)->first();
    }


    /**
     * Get the latest min ID we should be checking for new posts for
     *
     * @return string
     */
    public static function getMaxTimestamp() {
        return self::where('status', self::STATUS_COMPLETE)->max('max_timestamp');
    }

}
