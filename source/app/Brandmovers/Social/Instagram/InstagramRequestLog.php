<?php namespace Brandmovers\Social\Instagram;

use Illuminate\Database\Eloquent\Model;


class InstagramRequestLog extends Model
{
	const STATUS_RUNNING = "RUNNING";
	const STATUS_COMPLETE = "COMPLETED";
	const STATUS_FAILED = "FAILED";



	/**
	 * Get the most recent request made 
	 * 
	 * @return InstagramRequestLog
	 */
	public static function getLastRequest() {
		return self::orderBy('created_at', 'desc')->where('status', self::STATUS_COMPLETE)->first();
	}
	
	
	/**
	 * Get the latest min ID we should be checking for new posts for
	 * 
	 * @return string
	 */	
	public static function getLastId() {
		return self::max('min_tag_id');
	}

}
	