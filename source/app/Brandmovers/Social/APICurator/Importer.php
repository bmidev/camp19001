<?php namespace Brandmovers\Social\APICurator;

use GuzzleHttp\Client;
use App\Posts;

class Importer {
    public function import() {
        $isMore = true;
        //$client = new Client();


        $offset = 0;

        do{
            $apiResponse = json_decode(file_get_contents("https://api.curator.io/v1/feeds/c7356fc2-d935-465a-b8df-fc96eb525dad/posts/?limit=100&offset=$offset"), true);
            if(empty($apiResponse['posts'])){
                $isMore = false;
                break;
            }

            foreach($apiResponse['posts'] as $posts) {
                $postResponse = $this->savePost($posts);

                // TODO: Implement validation as per sweepstake after inserting all the data into the api_curators_imports table.

                echo ($postResponse['wasRecentlyCreated']) ? 'Added New Post: ' . $postResponse['post_id'] . '.' : 'Skipping ' . $postResponse['post_id'] . '.';
                echo "\n";
            }
            $offset+=100;
        }
        while($isMore);
//        $apiResponse = @json_decode($response->getBody(), true);


    }

    public function savePost($data) {
        
        $postData = [
            'post_id' => $data['id'],
            'source_identifier' => $data['source_identifier'],
            'source_created_at' => $data['source_created_at'],
            'network_name' =>  $data['network_name'],
            'url' => $data['url'],
            'user_url' => $data['user_url'],
            'text' => $data['text'],
            'image' => $data['image'],
            'image_thumbnail' => $data['thumbnail'], 
            'image_large' => $data['image_large'],
            'video' => $data['video'],
            'likes' => $data['likes'],
            'views' => $data['views'],
            'comments' => $data['comments'],
            'user_image' => $data['user_image'],
            'user_name' => $data['user_full_name'],
            'user_screen_name' => $data['user_screen_name']
        ];

        if($post = Posts::query()->where('post_id', $postData['post_id'])->first()){
            collect($postData)->except(['post_id'])->each(function($value, $key) use(&$post){
                $post->{$key} = $value;
            });

            $post->save();
            return $post;
        }
        
        return Posts::firstOrCreate($postData);
    }
}