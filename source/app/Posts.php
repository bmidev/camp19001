<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {
    protected $table = 'api_curator_imports';
    protected $fillable = [
        'post_id', 'source_identifier', 'source_created_at', 'network_name', 'url', 
        'user_url', 'text', 'image', 'image_thumbnail', 'image_large', 'video', 'likes', 'views', 'comments', 'user_image', 'user_name', 'user_screen_name'
    ];
}
