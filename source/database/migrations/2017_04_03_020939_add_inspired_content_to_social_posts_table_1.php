<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInspiredContentToSocialPostsTable1 extends Migration
{
    private $post_date;

    public function __construct()
    {
        $this->post_date = '2017-05-15 12:00:00';
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        \DB::statement("
            INSERT INTO `social_posts` (`id`, `network`, `network_id`, `url`, `media_type`, `image`, `image_thumb`, `video`, `title`, `comments`, `author_name`, `author_image`, `author_username`, `author_profile`, `post_date`, `reviewed`, `approved`, `show_gallery`, `contest_eligible`, `contest_winner`, `sweepstakes_winner`, `pepperidge_inspired`, `valid_entry`, `errors`, `created_at`, `updated_at`) VALUES
(NULL, 'Pepperidge', '1491199208', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020004-1d18494535d30ec6d8b6e05994fd5b73c5e56bdb.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020004-1d18494535d30ec6d8b6e05994fd5b73c5e56bdb.jpg', NULL, NULL, 'Bacon Egg Cheese', 'pepperidgefarm', '', '', '', '". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '', '". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199262', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020057-1fc18d3d25b846c4ba3edfdbed3d8ca178a06346.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020057-1fc18d3d25b846c4ba3edfdbed3d8ca178a06346.jpg', NULL, NULL, 'Blue', 'pepperidgefarm', '', '', '', '". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '', '". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199303', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020139-8acc70f4f83a5443a624cc9feba4f5494834a715.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020139-8acc70f4f83a5443a624cc9feba4f5494834a715.jpg', NULL, NULL, 'Caprese', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199347', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020223-e1ce2ae3382cc71f0f37b4ce246018b1a049b74e.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020223-e1ce2ae3382cc71f0f37b4ce246018b1a049b74e.jpg', NULL, NULL, 'Fondue', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199364', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020240-3d879c7f20e21b89c212bfe459f8ce53a48957fb.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020240-3d879c7f20e21b89c212bfe459f8ce53a48957fb.jpg', NULL, NULL, 'Italian', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199384', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020300-014a07e640f4e19c24ecbf31f1fb8a3234529253.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020300-014a07e640f4e19c24ecbf31f1fb8a3234529253.jpg', NULL, NULL, 'philly', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199406', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020322-d2646b7cc1a159d175e7177c6f3719e670b87062.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020322-d2646b7cc1a159d175e7177c6f3719e670b87062.jpg', NULL, NULL, 'Poblano', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."'),
(NULL, 'Pepperidge', '1491199443', '', 'image', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020359-5295326e287dc4b5c259a635505f116ee5a996a0.jpg', 'https://bm-projects-public.s3.amazonaws.com/camp17001/staging/inspired-content/20170403020359-5295326e287dc4b5c259a635505f116ee5a996a0.jpg', NULL, NULL, 'Tripple', 'pepperidgefarm', '', '', '','". $this->post_date ."', 1, 1, 1, 0, 0, 0, 1, 1, '','". $this->post_date ."', '". $this->post_date ."');

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('social_posts')
            ->where('network', 'Pepperidge')
            ->where('pepperidge_inspired', 1)
            ->whereDate('post_date', '=', $this->post_date)
            ->delete();
    }
}
