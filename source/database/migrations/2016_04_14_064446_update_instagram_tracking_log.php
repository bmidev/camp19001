<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInstagramTrackingLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Makes th tag_id fields strings instead of bigints
        DB::statement("ALTER TABLE `instagram_request_logs` CHANGE `min_tag_id` `min_tag_id` VARCHAR(255) NOT NULL, CHANGE `max_tag_id` `max_tag_id` VARCHAR(255) NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Makes th tag_id fields strings instead of bigints
        DB::statement("ALTER TABLE `instagram_request_logs` CHANGE `min_tag_id` `min_tag_id` BIGINT(20) NOT NULL, CHANGE `max_tag_id` `max_tag_id` BIGINT(20) NOT NULL;");
    }
}
