<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImportedColumnToCuratorImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('api_curator_imports', 'imported')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->tinyInteger('imported')->after('comments');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('api_curator_imports', 'imported')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->dropColumn('imported');
            });
        }
    }
}
