<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTintupRequestLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tintup_request_logs', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('request_date');
            $table->integer('max_timestamp');
            $table->enum('status', array('RUNNING', 'COMPLETED', 'FAILED', 'PAGED'))->default('RUNNING');
            $table->string('message', 255)->nullable()->default(NULL);
            $table->integer('records');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tintup_request_logs');
    }
}
