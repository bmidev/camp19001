<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreUserSpecificColumnsToCuratorTmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('api_curator_imports', 'user_image')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->text('user_image')->after('user_url');
            });
        }

        if (!Schema::hasColumn('api_curator_imports', 'user_name')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->string('user_name')->after('user_url');
            });
        }

        if (!Schema::hasColumn('api_curator_imports', 'user_screen_name')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->string('user_screen_name')->after('user_url');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('api_curator_imports', 'user_image')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->dropColumn('user_image');
            });
        }

        if (Schema::hasColumn('api_curator_imports', 'user_name')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->dropColumn('user_name');
            });
        }

        if (Schema::hasColumn('api_curator_imports', 'user_screen_name')) {
            Schema::table('api_curator_imports', function ($table) {
                $table->dropColumn('user_screen_name');
            });
        }
    }
}
