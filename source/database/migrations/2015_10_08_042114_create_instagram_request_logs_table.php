<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramRequestLogsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_request_logs', function(Blueprint $table)
        {
            $table->increments('id');

            $table->dateTime('request_date');
            $table->unsignedBigInteger('min_tag_id');
            $table->unsignedBigInteger('max_tag_id');
            $table->enum('status', array('RUNNING', 'COMPLETED', 'FAILED'))->default('RUNNING');
            $table->string('message', 255)->nullable()->default(NULL);
            $table->integer('records');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('instagram_request_logs');
    }

}
