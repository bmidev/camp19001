<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SocialPostWinnerFlags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_posts', function (Blueprint $table) {
            $table->boolean('contest_eligible')->default(false)->after('approved');
            $table->boolean('contest_winner')->default(false)->after('contest_eligible');
            $table->boolean('sweepstakes_winner')->default(false)->after('contest_winner');
            $table->boolean('pepperidge_inspired')->default(false)->after('sweepstakes_winner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_posts', function (Blueprint $table) {
            $table->dropColumn('contest_eligible');
            $table->dropColumn('contest_winner');
            $table->dropColumn('sweepstakes_winner');
            $table->dropColumn('pepperidge_inspired');
        });
    }
}
