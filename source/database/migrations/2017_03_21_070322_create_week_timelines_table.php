<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeekTimelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('week_timelines', function(Blueprint $table)
        {
            $table->increments('id');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->timestamps();
        });

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-05-14 12:00:00',
            'end'   => '2018-05-21 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-05-21 12:00:00',
            'end'   => '2018-05-28 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-05-28 12:00:00',
            'end'   => '2018-06-04 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-06-04 12:00:00',
            'end'   => '2018-06-11 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-06-11 12:00:00',
            'end'   => '2018-06-18 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-06-18 12:00:00',
            'end'   => '2018-06-25 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-06-25 12:00:00',
            'end'   => '2018-07-02 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-07-02 12:00:00',
            'end'   => '2018-07-09 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-07-09 12:00:00',
            'end'   => '2018-07-16 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-07-16 12:00:00',
            'end'   => '2018-07-23 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-07-23 12:00:00',
            'end'   => '2018-07-30 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-07-30 12:00:00',
            'end'   => '2018-08-06 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-08-06 12:00:00',
            'end'   => '2018-08-13 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-08-13 12:00:00',
            'end'   => '2018-08-20 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-08-20 12:00:00',
            'end'   => '2018-08-27 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        \DB::table('week_timelines')->insert([
            'start'   	=> '2018-08-27 12:00:00',
            'end'   => '2018-09-04 11:59:59',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('week_timelines');
    }
}
