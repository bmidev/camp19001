<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiCuratorImportsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('api_curator_imports', function (Blueprint $table) {
            $table->increments('id');

            // Post Information
            $table->string('post_id')->unique();
            $table->string('source_identifier');
            $table->string('source_created_at');
            $table->string('network_name', 50);
            $table->text('url');
            $table->text('user_url');
            $table->text('text');
            $table->text('image')->nullable();
            $table->text('image_thumbnail')->nullable();
            $table->text('image_large')->nullable();
            $table->text('video')->nullable();
            $table->string('likes');
            $table->string('views');
            $table->string('comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('api_curator_imports');
    }
}
