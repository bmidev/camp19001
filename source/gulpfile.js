var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    // SASS
    mix.sass('app.scss');
    
    // Javascript
    mix.scripts([

        // Vendor Scripts
        'jquery/dist/jquery.min.js',
        'jquery.stellar/jquery.stellar.min.js',
        'angular/angular.min.js',
        'angular-sanitize/angular-sanitize.min.js',
        'angular-timeago/dist/angular-timeago.min.js',
        'angular-resource/angular-resource.min.js',
        'angular-media-queries/match-media.js',
        'bxslider-4/dist/jquery.bxslider.min.js',
        'magnific-popup/dist/jquery.magnific-popup.min.js',
        'isotope/dist/isotope.pkgd.min.js',
        'angular-isotope/dist/angular-isotope.js',
        'ui-bootstrap-custom-build/ui-bootstrap-custom-0.14.0.min.js',
        'ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-0.14.0.min.js',

        'videogular/videogular.min.js',
        'videogular-controls/vg-controls.min.js',

        'slick-carousel/slick/slick.min.js',
        'angular-slick/dist/slick.min.js',

        'imagesloaded/imagesloaded.pkgd.min.js',
        //'scrollup/dist/jquery.scrollUp.js',
        // Site Scripts
        '../js/app.js',
        '../js/app/**'

    ], 'public/js/app.js', 'resources/assets/vendor/');




    // Admin SASS
    mix.sass([
        'admin/admin.scss'
    ], 'public/css/admin/admin.css');

    // Admin JS
    mix.scripts([

        // Vendor Scripts
        //'jquery/dist/jquery.min.js',
        //'magnific-popup/dist/jquery.magnific-popup.min.js',

        // Site Scripts
        '../js/admin/admin.js'

    ], 'public/js/admin/admin.js', 'resources/assets/vendor/');


    // Version
    //mix.version(['css/app.css', 'js/app.js', 'css/admin/admin.css', 'js/admin/admin.js']);

});
