app.controller('GalleryModalController', ['$scope', '$timeout', '$modalInstance', '$sce', 'items', 'index',
    function ($scope, $timeout, $modalInstance, $sce, items, index) {

    $scope.index = index;
    $scope.item = items[index];
    setVideoSrc($scope.item);


    $scope.showPrev = ($scope.index > 0) ? true : false;
    $scope.showNext = (items.length > ($scope.index+1)) ? true : false;


    $scope.next = function() {
        if( items.length > $scope.index) {
            $scope.item = items[++$scope.index];
            setVideoSrc($scope.item);
            $scope.showPrev = true;
            resetModal();
        }
        $scope.showNext = (items.length > ($scope.index+1)) ? true : false;
    };

    $scope.prev = function() {
        if( $scope.index > 0 ) {
            $scope.item = items[--$scope.index];
            setVideoSrc($scope.item);
            $scope.showNext = true;
            resetModal();
        }
        $scope.showPrev = ($scope.index > 0) ? true : false;
    };


    $scope.onPlayerReady = function(videoAPI) {
        $scope.videoAPI = videoAPI;
    };

    function setVideoSrc(item) {
        item.videoSrc = [{
            src: $sce.trustAsResourceUrl(item.video),
            type: "video/mp4"
        }];
    }

    function resetModal() {
        //$scope.videoAPI.stop();  // Bug fix, otherwise button will remain as "pause"
        if( $scope.item.media_type == "video" ) {
            $timeout(function() {
                $scope.$emit('video-resize');
            });
        }
    }

}]);