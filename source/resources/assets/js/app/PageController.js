app.controller('PageController', ['$scope', '$uibModal', function ($scope, $uibModal) {

    $scope.openRules = function($event) {
        $event.preventDefault();

        var modalInstance = $uibModal.open({
            templateUrl: BASE_URL + '/' + LOCALE + '/official-rules',
            size: 'lg'
        });
    }

}]);
