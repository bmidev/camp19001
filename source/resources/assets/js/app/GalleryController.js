app.controller('GalleryController', ['screenSize', '$scope', '$rootScope', '$window', '$timeout', '$uibModal', 'Gallery',
        function (screenSize, $scope, $rootScope, $window, $timeout, $uibModal, Gallery) {

        $scope.gallery = {
            items: {},
            maxTime: null,
            filter: null,
            pageSize: 8,
            hasMoreItems: true,
            expanded: false,
            loaded: false
        };

        var featuredItem = {
            //banner: {},
            //thumbnail: "./img/video-featured-thumb.png",
            //image: "./img/video-featured-thumb.png",
            //video: 'https://www.youtube.com/embed/Jz9fH1QT2VE',
            //photo: false,
            //featured: true,
            //media_type: 'featured',
            //date: '2015-10-01'
        };

        // Watch devices widths to alter gallery view
        $scope.desktop = screenSize.is('md, lg');
        $scope.mobile = screenSize.is('xs, sm');
        angular.element($window).bind('resize', function () {
            $scope.desktop = screenSize.is('md, lg');
            $scope.mobile = screenSize.is('xs, sm');
        });

        if( $scope.mobile ) {
            $scope.gallery.pageSize = 8;
        }


        /**
         * Request Social Posts based on current state params. Returns a promise
         *
         * @returns $promise
         */
        function loadItems() {
            // Request gallery items, automatically handle pagination request data
            return Gallery.page({
                filter: $scope.gallery.filter,
                maxTime: $scope.gallery.maxTime,
                pageSize: $scope.gallery.pageSize
            }, function(response) {
                $scope.gallery.maxTime = response.maxTime;
                $scope.gallery.hasMoreItems = (response.posts.length == $scope.gallery.pageSize);

                // Force expanded view if we don't have enough items to fill it
                if( !$scope.gallery.hasMoreItems ) {
                    $scope.gallery.expanded = true;
                }

                $scope.gallery.loaded = true;
            }).$promise;
        }


        /**
         * Initial item load for gallery
         *
         * @returns void
         */
        function init() {
            loadItems().then(function(response) {
                $scope.gallery.items = response.posts;

                // Add featured item as first in array
                if( $scope.gallery.filter == null || $scope.gallery.filter == 'video' ) {
                    //$scope.gallery.items.unshift(featuredItem);
                }
            })
        };
        init();  // Initial Data Load.  Trigger Now.


        /**
         * Load additional items to gallery
         *
         * @return void
         */
        $scope.loadMore = function() {
            loadItems().then(function(response) {
                // Append response to current items
                $scope.gallery.items = $scope.gallery.items.concat(response.posts);
            });

            $scope.gallery.expanded = true;
        };


        /**
         * Apply a filter to the gallery and reload gallery items
         *
         * @param filter
         */
        $scope.setFilter = function(filter) {
            $scope.gallery.filter = filter;
            $scope.gallery.maxTime = null;
            init();
        };


        /**
         * Update filter (from mobile select input)
         */
        $scope.updateFilter = function() {
            if( $scope.gallery.filter == '' ) {
                $scope.gallery.filter = null;
            }

            $scope.gallery.maxTime = null;
            $scope.gallery.showSlides = false;
            mobileGallerySet = false;

            init();
        };


        /**
         * Open gallery item modal
         *
         * @param item
         * @param index
         */
        $scope.openModal = function(item, index) {
            var modalInstance = $uibModal.open({
                templateUrl: BASE_URL + '/tpl/social-gallery-modal.html?v1.3',
                controller: 'GalleryModalController',
                size: 'lg',
                windowClass: 'center-modal',
                resolve: {
                    items: function () {
                        return $scope.gallery.items;
                    },
                    index: function() {
                        return index;
                    }
                }
            });

            modalInstance.opened.then(function() {
                $timeout(function() {
                    $rootScope.$broadcast('video-resize');
                }, 100);
            });
        };


        var url = encodeURIComponent('http://goldfishtales.com');
        var title = encodeURIComponent('Have a story about a time when Goldfish® crackers added smiles to your life?');
        $scope.twitter = 'http://twitter.com/home?status=' + title + '+' + url;
        $scope.facebook = 'http://www.facebook.com/share.php?u=' + url + '&title=' + title;
        $scope.instagram = 'https://instagram.com/goldfishsmiles/';


        /**
         * Mobile Slide Gallery
         */
        var mobileGallerySet = false;
        $scope.$watch('gallery.items', function(newVal, oldVal) {
            if( newVal.length > 0 && !mobileGallerySet ) {
                $scope.slides = chunk($scope.gallery.items, 6);
                $scope.gallery.showSlides = true;  // Don't trigger rendering until we have slides
                mobileGallerySet = true;
            }

        }, true);


        function chunk (arr, len) {
            var chunks = [],
                i = 0,
                n = arr.length;

            while (i < n) {
                var count = len;

                // Account for first item taking up two spots :)
                // ... but only if there is the 2 slot item prepended (which means no filters)
                if( i == 0 && ($scope.gallery.filter == null || $scope.gallery.filter == 'video') ) {
                    count--;
                }

                chunks.push(arr.slice(i, i += count));
            }

            return chunks;
        }

        $scope.getIndex = function(index, parentIndex) {
            var offset = 0;
            offset += (parentIndex) * 6;

            if( parentIndex > 0 && $scope.gallery.filter == null ) {
                offset--;
            }

            return index + offset;
        }

    }]
);