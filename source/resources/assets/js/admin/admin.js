
var idleTime = 0;
$(document).ready(function () {

    $('.datepicker').datepicker({
        autoclose: true
    });

    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});


function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 29) { // 30 minutes
        window.location = BASE_URL + '/admin/logout';
    }
}

