@extends('layout.admin')

@section('content')

    <div class="social-posts panel panel-default">
        <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> Social Posts</div>

        <form class="form-inline panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" style="padding-left:20px">
                        <label for="valid">Moderation:</label>
                        {!! Form::select('approved', ['all'=>'Any', 'approved'=>'Approved', 'rejected'=>'Rejected', 'pending'=>'Pending'], $filters['approved'], ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group" style="padding-left:20px">
                        <label for="valid">Valid:</label>
                        {!! Form::select('valid', ['all'=>'Any', 1=>'Valid', 0=>'Invalid'], $filters['valid'], ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group" style="padding-left:20px">
                        <label for="contest_eligible">Contest Eligible</label>
                        {!! Form::select('contest_eligible', ['all'=>'Any', 1=>'Yes', 0=>'No'], $filters['contest_eligible'], ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group" style="padding-left:20px">
                        <label for="media_type">Type</label>
                        {!! Form::select('media_type', ['all'=>'Any', 'photo'=>'Photo', 'video'=>'Video'], $filters['media_type'], ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" name="submit" value="search" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Search</button>
                    <button type="submit" name="submit" value="export" class="btn btn-default"><span class="glyphicon glyphicon-cloud-download"></span> Export CSV</button>
                    <button type="submit" name="submit" value="winner" class="btn btn-default"><span class="glyphicon glyphicon-star"></span> Random Winner</button>
                </div>
            </div>
        </form>

        <hr />

        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-xs-2" class="text-center">Image</th>
                <th class="col-xs-5">Content</th>
                <th class="col-xs-2 text-center">Gallery</th>
                <th class="col-xs-2 text-center">Contest</th>
                <th class="col-xs-1 text-center">Status</th>
            </tr>
            </thead>
            <tbody>
            @if( count($posts) == 0 )
                <tr>
                    <td colspan="6" class="no-matches">No Matching Posts</td>
                </tr>
            @else
                @foreach( $posts as $p )
                    <tr class="{{ $p->valid_entry === 0 ? "danger" : ""}}">
                        <td>
                            @if( $p->isImage() )
                                <a class="social-image" href="{{ $p->image }}">
                                    <img src="{{ $p->image }}" alt="" />
                                </a>
                            @elseif( $p->isVideo() )
                                <a class="social-image" href="{{ $p->image }}">
                                    <video width="320" height="320" controls>
                                        <source src="{{ $p->video }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </a>
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            <img class="author-image" src="{{ $p->author_image }}" alt="" />
                            <a href="{{ $p->author_profile }}" target="_blank">{{ $p->author_username }}</a>
                            ({{ $p->author_name }})
                            <hr/>
                            {{ $p->comments }}
                            <hr/>
                            <a href="{{ $p->url }}" target="_blank">View on {{ $p->getNetwork() }}</a><br/>
                            {{ date("m/d/y g:i A", strtotime($p->post_date)) }}
                        </td>
                        <td class="text-center">
                            <label>
                                <input type="checkbox" value="1" class="show-gallery" {{ $p->show_gallery ? "checked=''" : "" }} /><br/>
                                Gallery
                            </label>
                            <br/><br/><br/>

                            <label>
                                <input type="checkbox" name="sweepstakes_winner" value="1" {{ $p->sweepstakes_winner ? "checked=''" : "" }} /><br/>
                                Sweepstakes Winner
                            </label>
                        </td>
                        <td class="text-center">
                            <label>
                                <input type="checkbox" value="1" class="contest-eligible" {{ $p->contest_eligible ? "checked=''" : "" }} /><br/>
                                Contest
                            </label>
                            <br/><br/><br/>

                            <label>
                                <input type="checkbox" name="contest_winner" value="1"  {{ $p->contest_winner ? "checked=''" : "" }} /><br/>
                                Contest Winner
                            </label>
                        </td>
                        <td class="text-center">
                            @if( !$p->reviewed )
                                Not Reviewed!
                            @endif

                            @if( $p->valid_entry )
                                Valid Entry
                            @else
                                Invalid -
                                {{ $p->errors }}
                            @endif

                            <br/><br/>
                            <div class="btn btn-default btn-block update-entry" data-id="{{ $p->id }}">Update Entry</div>
                            <div class="row-updated text-success" style="display:none"><strong>Post Updated!</strong></div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

        @if( $submitAction != "winner" )
            <div class="pagination-container">
                <?= $posts->appends(Input::only(['approved', 'valid', 'contest_eligible', 'media_type']))->render() ?>
            </div>
        @endif
    </div>

@stop


@section('scripts')
    <script>
        $(function() {

            // Lightbox
            $('.social-posts table').magnificPopup({
                delegate: '.social-image',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });


            $('.update-entry').on('click', function() {
                var id = $(this).attr('data-id'),
                    $row = $(this).closest('tr'),
                    sweepsWinner = $row.find('input[name=sweepstakes_winner]').is(':checked') ? 1 : 0,
                    contestWinner = $row.find('input[name=contest_winner]').is(':checked') ? 1 : 0,
                    showGallery = $row.find('.show-gallery').is(':checked'),
                    contestEligible = $row.find('.contest-eligible').is(':checked');

                $.post(BASE_URL + '/admin/posts/' + id + '/winners', {
                    sweepstakes_winner: sweepsWinner,
                    contest_winner: contestWinner,
                    show_gallery: showGallery ? 1 : 0,
                    contest_eligible: contestEligible ? 1 : 0,
                    _token: _token
                }, function(response) {
                    $row.find('.row-updated').fadeIn();
                    setTimeout(function() {
                        $row.find('.row-updated').fadeOut();
                    }, 2000);

                });
            })

        })

    </script>
@stop
