@extends('layout.admin')

@section('content')

	<div class="panel panel-default">
		<div class="panel-heading"><span class="glyphicon glyphicon-calendar"></span> Filter by Date</div>
		<div class="panel-body">
			{!! Form::open(['method' => 'GET']) !!}
				Date Range:
				{!! Form::text('start_date', $filters['startDate']->format('m/d/Y'), ['class' => 'datepicker']) !!}
				-
				{!! Form::text('end_date', $filters['endDate']->format('m/d/Y'), ['class' => 'datepicker']) !!}
				<button type="submit" class="btn btn-primary">
					<i class="glyphicon glyphicon-search"></i>
					Search
				</button>
			{!! Form::close() !!}
		</div>
		<div class="panel-heading"><span class="glyphicon glyphicon-cloud"></span> Entries by Network</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<table class="table table-striped">
						<thead>
						<th>Total Entries:</th>
						<th class="text-right">{{ number_format($totalEntries) }}</th>
						</thead>
						<tbody>
						@foreach( $networkSummary as $m )
							<tr>
								<td>{{ ucfirst($m['network']) }}</td>
								<td class="text-right">{{ number_format($m['total']) }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-6 text-center">
					<canvas id="methods-chart" width="175" height="175"></canvas>
					<div id="chart-legend" class="chart-legend"></div>
				</div>
			</div>
		</div>
		<div class="panel-heading"><span class="glyphicon glyphicon-picture"></span> Entries by Media Type</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<table class="table table-striped">
						<thead>
						<th>Media Types:</th>
						<th></th>
						</thead>
						<tbody>
						@foreach( $mediaTypeSummary as $m )
							<tr>
								<td>{{ ucfirst($m['media_type']) }}</td>
								<td class="text-right">{{ number_format($m['total']) }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-6 text-center">
					<canvas id="media-chart" width="175" height="175"></canvas>
					<div id="media-chart-legend" class="chart-legend"></div>
				</div>
			</div>
		</div>
		<div class="panel-heading"><span class="glyphicon glyphicon-signal"></span> Analytics Summary</div>
		<table class="table">
			<tr>
				<thead>
				<th>Date</th>
				<th class="text-center">Visitors</th>
				<th class="text-center">Page Views</th>
				</thead>
			</tr>
			@foreach( $analytics['pageViews'] as $p )
				<tr>
					<td>{{ $p['date']->format('m-d-Y') }}</td>
					<td class="text-center">{{ $p['visitors'] }}</td>
					<td class="text-center">{{ $p['pageViews'] }}</td>
				</tr>
			@endforeach
		</table>
	</div>

@stop


@section('scripts')
	<script>
		$(function() {

			// Network Summary Graph
			var data = [
				@foreach( $networkSummary as $key => $m )
				{
					value: {{ $m['total'] }},
					label: '{{ ucfirst($m['network']) }}',
					color: "{{ $chartColors['color'][$key] }}",
					highlight: "{{ $chartColors['highlight'][$key] }}",
				},
				@endforeach
            ];
			var options = {};

			var ctx = $('#methods-chart').get(0).getContext("2d");
			var donutChart = new Chart(ctx).Doughnut(data,options);

			$('#chart-legend').html(donutChart.generateLegend());


			// Media Type Graph
			var data = [
				@foreach( $mediaTypeSummary as $key => $m )
				{
					value: {{ $m['total'] }},
					label: '{{ ucfirst($m['media_type']) }}',
					color: "{{ $chartColors['color'][$key] }}",
					highlight: "{{ $chartColors['highlight'][$key] }}",
				},
				@endforeach
            ];
			var options = {};

			var ctx = $('#media-chart').get(0).getContext("2d");
			var donutChart = new Chart(ctx).Doughnut(data,options);

			$('#media-chart-legend').html(donutChart.generateLegend());


			// Lightbox
			$('.social-posts table').magnificPopup({
				delegate: '.social-image',
				type: 'image',
				gallery: {
					enabled: true
				}
			});

		})

	</script>
@stop
