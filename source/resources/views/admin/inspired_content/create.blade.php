@extends('layout.admin')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Add Inspired Content
        </div>
        <div class="panel-body">
            <h3>Add Inspired Content</h3>

            {!! \Html::ul($errors->all()) !!}

            {!! Form::open(['url' => route('admin.inspired_content.store'), 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="form-group">
                {!! Form::label('comments', 'Comments', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::textarea('comments', Input::old('comments'), ['class' => 'form-control', 'required'=>'']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('video', 'Video', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::file('video', Input::old('video_url'), array('class' => 'form-control')) !!}
                    <div class="help-block">
                        (Videos should be .MP4 files with H264 encoding)
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('photo', 'Photo', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::file('photo', Input::old('photo_url'), array('class' => 'form-control')) !!}
                    <div class="help-block">
                        (When adding videos, this photo will appear as the preview image)
                    </div>
                </div>
            </div>

            <div class="row form-footer">
                <div class="col-10-sm col-sm-offset-2">
                    {!! Form::submit('Add Post', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection