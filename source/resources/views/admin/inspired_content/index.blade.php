@extends('layout.admin')

@section('content')

    <div class="social-posts panel panel-default">
        <div class="panel-heading"><span class="glyphicon glyphicon-asterisk"></span> Inspired Content</div>
        <div class="panel-body text-right">
            <a href="{{ route('admin.inspired_content.create') }}" class="btn btn-primary">
                <i class="fa fa-plus"></i>
                Add New Inspired Content
            </a>
        </div>

        <hr>

        <table class="table table-striped clearfix">
            <thead>
            <tr>
                <th class="col-xs-2" class="text-center">Image</th>
                <th class="col-xs-4">Content</th>
                <th class="col-xs-2">Post Date</th>
                <th class="col-xs-2">Actions</th>
            </tr>
            </thead>
            <tbody>
            @if( count($posts) == 0 )
            <tr>
                <td colspan="6" class="no-matches">No Matching Posts</td>
            </tr>
            @else
            @foreach( $posts as $p )
            <tr class="{{ $p->valid_entry === 0 ? "danger" : ""}}">
            <td>
                @if( $p->isImage() )
                <a class="social-image" href="{{ $p->image }}">
                    <img src="{{ $p->image }}" alt="" />
                </a>
                @elseif( $p->isVideo() )
                <a class="social-image" href="{{ $p->image }}">
                    <video width="320" height="320" controls>
                        <source src="{{ $p->video }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </a>
                @else
                N/A
                @endif
            </td>
            <td>
                {!! nl2br($p->comments) !!}
            </td>
            <td>
                {{ date("m/d/y g:i A", strtotime($p->post_date)) }}
            </td>
            <td>
                <a href="{{ route('admin.inspired_content.edit', $p) }}" class="btn btn-primary">Edit</a>
                {!! Form::open(['route' => ['admin.inspired_content.destroy', $p->id], 'method' => 'delete', 'class' => 'inline-block']) !!}
                <button type="submit" class="btn btn-danger btn-mini">Delete</button>
                {!! Form::close() !!}
            </td>
            </tr>
            @endforeach
            @endif
            </tbody>
        </table>

    </div>
@stop
