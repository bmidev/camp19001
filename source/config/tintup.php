<?php

// Example Request: https://api.tintup.com/v1/feed/goldfishsmiles?api_token=789953e3a7637e14cab8af232c467b1bcce97004

return [
    'url' => env('TINT_URI', 'https://api.tintup.com/v1/feed/'),
    'tint_name' => env('TINT_NAME', 'respectthebunpromotion'),
    'api_token' => env('TINT_API_TOKEN', '789953e3a7637e14cab8af232c467b1bcce97004'),
    'api_limit' => 300,
];